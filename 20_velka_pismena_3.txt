Nad postelí mám mapu.
Ale není jen tak obyčejná.
Je seškrabovací.
Všechna místa, kde jsem byl, si na ni mohu označit.
Ležím v posteli s očima upřenýma na mapu.
Už se vidím jako námořník brázdící moře a oceány od severu k jihu.
Projíždím prosluněné Jaderské moře, zabrousím do moře Egejského, proplouvám úžiny Bospor a Dardanely.
Najednou jsem uprostřed širého Atlantského oceánu.
Blíží se tajfun. Honem hledám, jestli není poblíž nějaký ostrov, třeba Kanárské ostrovy nebo souostroví Baleáry.
Ty názvy jsem už někdy slyšel, ale nevím, kde leží. Možná jinde.
Lodivode, zkrátka musíš vytrvat!